$(function() {
      $('.back-to-top').click(function() {
        $('html, body').animate({scrollTop: 0}, 100);
         return false;
     });

     $(window).scroll(function() {
       if($(this).scrollTop() > 230) {
        $('.back-to-top').addClass('active');
       }
       else {
        $('.back-to-top').removeClass('active');
       }
     });

     $(".nav__open").on('click', function() {
       $(this).closest('.mobile-nav-toggle').addClass('isNavOpened');
       $('.navbar ul').slideToggle('fast');
     });

     $(".nav__close").on('click', function() {
      $('.mobile-nav-toggle').removeClass('isNavOpened');
      $('.navbar ul').slideUp('fast');
    });

    $(window).resize(function(){
      $('.mobile-nav-toggle').removeClass('isNavOpened');
      $('.navbar ul').attr('style','');
    });
});